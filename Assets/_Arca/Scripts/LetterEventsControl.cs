﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterEventsControl : MonoBehaviour {

    private static LetterEventsControl _instance;
    public static LetterEventsControl Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LetterEventsControl>();

            return _instance;
        }
    }

    public EventSelector[] Events { get; set; }

    public bool WordSpawned { get; set; }
    public int LetterIndex { get; set; }

    void Start ()
    {
        Events = GetComponentsInChildren<EventSelector>(true);
	}
	
	void Update ()
    {
		
	}

    public void EnableEvents()
    {
        for (int i = 0; i < Events.Length; i++)
        {
            Events[i].gameObject.SetActive(true);
        }
    }

    public void DisableEvents()
    {
        for (int i = 0; i < Events.Length; i++)
        {
            Events[i].gameObject.SetActive(false);
        }
    }
}
