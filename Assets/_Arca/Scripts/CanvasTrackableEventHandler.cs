﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using EasyAR;

public class CanvasTrackableEventHandler : MonoBehaviour
{
    [System.Serializable]
    public class TrackerEvent : UnityEvent { }
    public TrackerEvent OnFound = new TrackerEvent();
    public TrackerEvent OnLost = new TrackerEvent();

    public bool IsFound { get; private set; }
    public bool EnableOnFound = true;
    public bool DisableOnLost = true;

    public ImageTargetBehaviour Target
    {
        get
        {
            return GetComponent<ImageTargetBehaviour>();
        }
    }

    void Start()
    {
        Target.TargetFound += (a) => OnTrackingFound();
        Target.TargetLost += (a) => OnTrackingLost();
    }

    public void OnTrackingFound()
    {
        IsFound = true;
        OnFound.Invoke();

        if (!EnableOnFound)
            return;

        EnableComponents();
    }


    public void OnTrackingLost()
    {
        IsFound = false;

#if UNITY_EDITOR
        OnLost.Invoke();
#else
        StartCoroutine(TimeToLost());
#endif

        if (!DisableOnLost)
            return;

        DisableComponents();
    }

    public void EnableComponents()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
        Canvas[] canvasComponents = GetComponentsInChildren<Canvas>(true);

        foreach (Renderer component in rendererComponents)
            component.enabled = component != GetComponent<Renderer>();

        foreach (Collider component in colliderComponents)
            component.enabled = true;

        foreach (Canvas component in canvasComponents)
            component.enabled = true;
    }

    public void DisableComponents()
    {
        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
        Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);
        Canvas[] canvasComponents = GetComponentsInChildren<Canvas>(true);

        foreach (Renderer component in rendererComponents)
            component.enabled = false;

        foreach (Collider component in colliderComponents)
            component.enabled = false;

        foreach (Canvas component in canvasComponents)
            component.enabled = false;
    }

    IEnumerator TimeToLost()
    {
        float elapsedTime = 0;

        while (elapsedTime < 1)
        {
            elapsedTime += Time.deltaTime;

            if (IsFound)
                yield break;

            yield return null;
        }

        if (!IsFound)
            OnLost.Invoke();
    }
}
