﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAR;

public class WordSpawner : MonoBehaviour {

    public int LetterIndex;
    public bool LetterIsUpper;
    public Transform WordContainer;

    private List<GameObject> _word;
    private ImageTargetBehaviour _trackEvent;
    private EventSelector _eventSelector;

    private List<string> _listOfWords = new List<string>();
    private List<string> _currentListOfWords = new List<string>();

    public void Initialize ()
    {
        _trackEvent = GetComponent<ImageTargetBehaviour>();
        _eventSelector = GetComponentInChildren<EventSelector>();

        _word = new List<GameObject>();

        _listOfWords = WordsTSVReader.Instance.GetWords(LetterIndex);
        _currentListOfWords = new List<string>(_listOfWords);

        _eventSelector.OnClick.AddListener(() =>
        {
            SpawnFirstWord();
            LetterEventsControl.Instance.DisableEvents();
            LetterEventsControl.Instance.WordSpawned = true;
            LetterEventsControl.Instance.LetterIndex = LetterIndex;
        });

        _trackEvent.TargetFound += (b) =>
        {
            if (LetterEventsControl.Instance.WordSpawned)
                _eventSelector.gameObject.SetActive(false);
            else
                _eventSelector.gameObject.SetActive(true);
        };

        _trackEvent.TargetLost += (b) =>
        {
            if(LetterEventsControl.Instance.WordSpawned && LetterEventsControl.Instance.LetterIndex == LetterIndex)
            {
                ClearWords();
                LetterEventsControl.Instance.WordSpawned = false;
                LetterEventsControl.Instance.EnableEvents();
            }
        };

        WordContainer = Instantiate(WordContainer);
        WordContainer.parent = transform;
        WordContainer.localPosition = Vector3.zero;
        WordContainer.localScale = Vector3.one;
    }

    void Update ()
    {
		
	}

    private void SpawnWord(string word)
    {
        _word = new List<GameObject>();
        word = LetterIsUpper ? word.ToUpper() : word.ToLower();

        float zPosition = 0;
        float newLetterPos = 0;
        float oldLetterPos = 0;
        Material wordMaterial = null;

        for (int i = 0; i < word.Length; i++)
        {
            char letter = word[i];

            if (LetterIsUpper)
            {
                GameObject lt = LetterSpawner.Instance.SpawnUpperLetter(letter.ToString());

                if (lt == null)
                    continue;

                _word.Add(lt);
            }
            else
            {
                GameObject lt = LetterSpawner.Instance.SpawnLowerLetter(letter.ToString());

                if (lt == null)
                    continue;

                _word.Add(lt);
            }

            _word[i].transform.parent = WordContainer;
            _word[i].transform.localPosition = new Vector3(0.2f, 0.55f, 0);
            _word[i].transform.localEulerAngles = new Vector3(-50, 90, 0);
            _word[i].transform.localScale = Vector3.one;
            _word[i].GetComponent<LetterButton>().OnClick += SpawnRandomWord;
            _word[i].GetComponent<LetterButton>().OnUp += WordContainer.GetComponent<LetterClickAnimation>().Up;
            _word[i].GetComponent<LetterButton>().OnDown += WordContainer.GetComponent<LetterClickAnimation>().Down;

            if (i > 0)
            {
                _word[i].GetComponent<MeshRenderer>().material = wordMaterial;

                newLetterPos = _word[i].GetComponent<MeshFilter>().mesh.bounds.extents.x;
                
                zPosition += (((newLetterPos + oldLetterPos)) + 0.05f);

                _word[i].transform.localPosition = new Vector3(0.2f, 0.55f, zPosition);
                oldLetterPos = newLetterPos;


                PotaTween.Create(_word[i]).SetScale(Vector3.one * 0.5f, Vector3.one).SetAlpha(0f, 1f).SetDuration(0.5f).SetDelay(1f + (i * 0.15f)).SetEaseEquation(Ease.Equation.OutBack).Play();
            }
            else
            {
                wordMaterial = _word[i].GetComponent<MeshRenderer>().material;

                _word[i].transform.localPosition = new Vector3(0.2f, 0.55f, zPosition);
                newLetterPos = _word[i].GetComponent<MeshFilter>().mesh.bounds.extents.x;
                oldLetterPos = newLetterPos;

                Vector3 initialPosition = new Vector3(0.33f, 0.155f, -0.033f);
                Vector3 initialRotation = new Vector3(-90, 90, 0);
                Vector3 finalPosition = new Vector3(0.2f, 0.55f, 0);
                Vector3 finalRotation = new Vector3(-50, 90, 0);

                PotaTween.Create(_word[i]).SetPosition(initialPosition, finalPosition, true).SetRotation(initialRotation, finalRotation, true).SetDuration(1.5f).SetEaseEquation(Ease.Equation.OutBack).Play();
            }
        }
    }

    public void SpawnFirstWord()
    {
        SpawnWord(_currentListOfWords[0]);
        _currentListOfWords.RemoveAt(0);

        _eventSelector.gameObject.SetActive(false);
    }

    public void SpawnRandomWord()
    {
        DeleteWord();

        if (_currentListOfWords.Count <= 0)
            ResetWords();

        int index = Random.Range(0, _currentListOfWords.Count);

        SpawnWord(_currentListOfWords[index]);
        _currentListOfWords.RemoveAt(index);
    }

    public void ResetWords()
    {
        _currentListOfWords = new List<string>(_listOfWords);
    }

    public void DeleteWord()
    {
        for (int i = 0; i < _word.Count; i++)
            Destroy(_word[i]);

        _word = new List<GameObject>();
    }

    public void ClearWords()
    {
        DeleteWord();
        ResetWords();

        _eventSelector.gameObject.SetActive(true);
    }
}
