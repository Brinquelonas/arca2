﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

[System.Serializable]
public struct AnimalAnimation
{
    public string Name;
    public SkeletonGraphic Graphic;
    public AudioClip Clip;
    public AudioClip[] TextsClips;
}

public class AnimalAnimations : MonoBehaviour
{
    public List<AnimalAnimation> Animations = new List<AnimalAnimation>();

    public void EnableAnimation(string name)
    {
        for (int i = 0; i < Animations.Count; i++)
        {
            Animations[i].Graphic.gameObject.SetActive(Animations[i].Name == name);

            if (Animations[i].Graphic.gameObject.activeSelf)
                Animations[i].Graphic.AnimationState.SetAnimation(0, "animation", true);
        }
    }

    public AudioClip GetAudioClip(string name)
    {
        AudioClip clip = null;

        for (int i = 0; i < Animations.Count; i++)
        {
            if (Animations[i].Name == name)
            {
                clip = Animations[i].Clip;
                return clip;
            }
        }

        return clip;
    }

    public AudioClip GetTextsAudioClip(string name, int index)
    {
        AudioClip clip = null;

        for (int i = 0; i < Animations.Count; i++)
        {
            if (Animations[i].Name == name)
            {
                if(index < Animations[i].TextsClips.Length)
                {
                    clip = Animations[i].TextsClips[index];
                    return clip;
                }
            }
        }

        return clip;
    }
}
