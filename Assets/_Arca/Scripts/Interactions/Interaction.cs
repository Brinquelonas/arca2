﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interaction : MonoBehaviour {

    [System.Serializable]
    public class InteractionEvent : UnityEvent<bool> { }

    public InteractionEvent OnFinish = new InteractionEvent();

    protected int CurrentSpace;

    public void StartInteraction(int spaceIndex)
    {
        CurrentSpace = spaceIndex;
    }

}
