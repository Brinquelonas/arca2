﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TrashItem : DraggableElement {

    [System.Serializable]
    public class TrashCollisionEvent : UnityEvent<TrashItem, Collider2D> { }

    public TrashCollisionEvent OnTriggerEnterEvent = new TrashCollisionEvent();
    public TrashCollisionEvent OnTriggerExitEvent = new TrashCollisionEvent();

    public bool Colliding { get; set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnTriggerEnterEvent.Invoke(this, collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        OnTriggerExitEvent.Invoke(this, collision);
    }

}
