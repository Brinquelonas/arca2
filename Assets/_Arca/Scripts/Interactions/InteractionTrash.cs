﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionTrash : Interaction {

    public List<TrashItem> DragItems;
    public Image Trash;

    private void Start()
    {
        for (int i = 0; i < DragItems.Count; i++)
        {
            InitializeItem(i);
        }
    }

    private void OnEnable()
    {
        for (int i = 0; i < DragItems.Count; i++)
        {
            DragItems[i].gameObject.SetActive(true);
        }
    }

    private void InitializeItem(int index)
    {
        DragItems[index].OnBegin.AddListener(OnItemDragBegin);
        DragItems[index].OnEnd.AddListener(OnItemDragEnd);
        DragItems[index].OnTriggerEnterEvent.AddListener(OnItemTriggerEnter);
        DragItems[index].OnTriggerExitEvent.AddListener(OnItemTriggerExit);
    }

    public void CheckCompletion()
    {
        for (int i = 0; i < DragItems.Count; i++)
        {
            if (DragItems[i].gameObject.activeSelf)
                return;
        }

        OnFinish.Invoke(true);
    }

    public void OnItemDragBegin(DraggableElement item)
    {
        item.StartPosition = item.transform.position;
    }

    public void OnItemDragEnd(DraggableElement item)
    {
        item.transform.position = item.StartPosition;

        if (((TrashItem)item).Colliding)
        {
            OnItemTriggerExit((TrashItem)item, Trash.GetComponent<Collider2D>());
            item.gameObject.SetActive(false);
        }

        CheckCompletion();
    }

    public void OnItemTriggerEnter(TrashItem item, Collider2D collision)
    {
        if (collision.gameObject == Trash.gameObject)
        {
            item.Colliding = true;
        }
    }

    public void OnItemTriggerExit(TrashItem item, Collider2D collision)
    {
        if (collision.gameObject == Trash.gameObject)
        {
            item.Colliding = false;
        }
    }
}
