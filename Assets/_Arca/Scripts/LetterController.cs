﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterController : MonoBehaviour {

    public GameObject[] LetterObjects;

	public void EnableLetter(string letter)
    {
        for (int i = 0; i < LetterObjects.Length; i++)
        {
            LetterObjects[i].SetActive(LetterObjects[i].name == letter);
        }
    }

}
