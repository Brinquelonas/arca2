﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum LettersToComplete
{
    None,
    Vowels,
    Consonants,
    Encounters,
    All
}

public class SeaGameController : MonoBehaviour {

    public TSVReader TSVReader;
    public string Word;
    public int ChanceOfWordLetter = 75;
    public Text HeaderText;
    public Text WordCountText;
    public SeaGameBoat Boat;
    public SeaGameHint Hint;
    public SeaGameLetterHolders Holders;
    public Transform DraggingLettersHolder;
    public SeaLetterSpawner LetterSpawner;
    public LettersToComplete LettersToComplete;

    [Header("Prefabs")]
    public DraggableLetter LetterPrefab;
    public SeaLetter LetterHolderPrefab;
    public LetterSlot LetterSlotPrefab;

    [Header("Report")]
    public ReportScroll ReportScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;
    [SerializeField]
    private Color _rightColor = Color.green;
    public string RightColor
    {
        get
        {
            return ColorUtility.ToHtmlStringRGBA(_rightColor);
        }
    }

    [SerializeField]
    private Color _wrongColor = Color.red;
    public string WrongColor
    {
        get
        {
            return ColorUtility.ToHtmlStringRGBA(_wrongColor);
        }
    }

    private List<Sprite> _wordSprites = new List<Sprite>();
    private List<string> _words = new List<string>();

    private int _wordsCompleted;

    private string _report;
    public string Log
    {
        get
        {
            return _report;
        }
    }

    private int _wrongAnswers;
    private int _rightAnswers;
    private int _wordWrongAnswers;
    private int _wordRightAnswers;

    private System.DateTime _questionStartTime;
    private System.DateTime _questionEndTime;
    private System.TimeSpan _gameTotalTime;
    private System.DateTime _gameStartTime;
    private System.DateTime _gameEndTime;

    public List<SeaLetter> LetterHolders
    {
        get
        {
            return Holders.List;//new List<SeaLetter>(FindObjectsOfType<SeaLetter>());
        }
    }

    private static SeaGameController _instance;
    public static SeaGameController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SeaGameController>();

            return _instance;
        }
    }

    void Start()
    {
        HeaderText.text = "DIGITE O NOME DO JOGADOR";

        if (GameConfigs.LettersToComplete != LettersToComplete.None)
            LettersToComplete = GameConfigs.LettersToComplete;

        _wordSprites = new List<Sprite>(Resources.LoadAll<Sprite>("SeaOfLetters"));

        _words = TSVReader.Words;
        /*for (int i = 0; i < _wordSprites.Count; i++)
        {
            switch (LettersToComplete)
            {
                default:
                case LettersToComplete.None:
                case LettersToComplete.All:
                case LettersToComplete.Vowels:
                    foreach (var s in _wordSprites)
                    {
                        _words.Add(s.name);
                    }
                    break;
                case LettersToComplete.Consonants:
                    int count = 0;
                    string letters = "bcdfghjklmnpqrstvwxyz";
                    for (int j = 0; j < _wordSprites[i].name.Length; j++)
                    {
                        if (letters.Contains(_wordSprites[i].name[j].ToString()))
                            count++;
                    }
                    if (count > 1)
                        _words.Add(_wordSprites[i].name);
                    break;
                case LettersToComplete.Encounters:
                    bool hasEncounter = false;
                    for (int j = 0; j < _wordSprites[i].name.Length; j++)
                    {
                        if ((j < _wordSprites[i].name.Length - 1 && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j].ToString()) && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j + 1].ToString()))
                        || (j < _wordSprites[i].name.Length - 1 && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j].ToString()) && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j + 1].ToString()))
                        || (j > 0 && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j].ToString()) && "aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j - 1].ToString()))
                        || (j > 0 && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j].ToString()) && !"aeiouAEIOUáãéêíóõôúÁÃÉÊÍÓÕÔÚ".Contains(_wordSprites[i].name[j - 1].ToString())))
                        //continue;
                        {
                            hasEncounter = true;
                            break;
                        }
                    }
                    if (hasEncounter)
                        _words.Add(_wordSprites[i].name);
                    break;
            }
        }*/
    }

	public void StartGame()
    {
        LogPlay("Nível " + GameConfigs.GameLevel + "\n" + GameConfigs.PlayerName + "\n");

        Boat.gameObject.SetActive(true);
        PotaTween.Get(Boat.gameObject, 1).Play(() => 
        {
            Hint.gameObject.SetActive(true);
            GetNewWord();
        });

        Holders.gameObject.SetActive(true);

        if (LettersToComplete == LettersToComplete.All)
        {
            for (int i = 0; i < Holders.List.Count; i++)
            {
                Destroy(Holders.List[i].gameObject);
            }
            Holders.List = new List<SeaLetter>();

            //LetterSpawner.SpawnLetter("a");
            LetterSpawner.StartSpawning();
        }

        _gameStartTime = System.DateTime.Now;
        WordCountText.text = _wordsCompleted.ToString("00") + "/10";
    }

    public void CheckWordCompletion()
    {
        for (int i = 0; i < Boat.LetterSlots.Count; i++)
        {
            if (!Boat.LetterSlots[i].IsFilled)
                return;
        }
        _wordsCompleted++;
        Boat.ClearSlots();

        _questionEndTime = System.DateTime.Now;
        System.TimeSpan time = _questionEndTime.Subtract(_questionStartTime);

        string t = string.Format("{0:D2}m {1:D2}s",
        (int)time.TotalMinutes,
        time.Seconds);
        LogPlay("\nTempo de resposta: " + t);

        HeaderText.text = "CORRETO!";
        WordCountText.text = _wordsCompleted.ToString() + "/10";

        LogPlay("\nTentativas corretas: " + _wordRightAnswers.ToString() + "\nTentativas erradas: " + _wordWrongAnswers.ToString() + "\n");

        if (!CheckEndGame())
            Invoke("GetNewWord", 1f);
    }

    public bool CheckEndGame()
    {
        if (_wordsCompleted >= 10)
        {
            EndGame();
            return true;
        }
        return false;
    }

    public void GetNewWord()
    {
        HeaderText.text = "COMPLETE A PALAVRA";

        //Sprite sprite = _wordSprites[Random.Range(0, _wordSprites.Count)];
        //Word = sprite.name;
        Word = _words[Random.Range(0, _words.Count)];
        Sprite sprite = null;
        for (int i = 0; i < _wordSprites.Count; i++)
        {
            if (_wordSprites[i].name == Word)
            {
                sprite = _wordSprites[i];
                break;
            }
        }
        Boat.CreateSlots(Word, LettersToComplete);
        Hint.SetImage(sprite);

        if(LettersToComplete != LettersToComplete.Vowels)
        {
            List<SeaLetter> list = new List<SeaLetter>(LetterHolders);

            int n = list.Count;
            System.Random rng = new System.Random();
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                SeaLetter value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            for (int i = 0; i < list.Count; i++)
                list[i].Value = string.Empty;
            for (int i = 0; i < list.Count; i++)
                list[i].CreateNewLetter(false);
        }

        _wordWrongAnswers = 0;
        _wordRightAnswers = 0;

        LogPlay("\nPalavra: " + Word.ToUpper() + "\n");

        _questionStartTime = System.DateTime.Now;
    }

    public string GetNewLetter()
    {
        switch (LettersToComplete)
        {
            case LettersToComplete.None:
            case LettersToComplete.All:
            default:
                return GetAnyLetter();
            case LettersToComplete.Vowels:
                return GetAnyLetter("aeiou");
            case LettersToComplete.Consonants:
                return GetAnyLetter("bcdfghjklmnpqrstvwxyz");
            case LettersToComplete.Encounters:
                return GetAnyLetter();
        }
    }    
    
    private string GetNewEncounter()
    {
        return null;
    }

    private string GetSimilarLetters(string l = "abcdefghijklmnopqrstuvwxyzçáãéíóõú")
    {
        string letters = string.Empty;

        if (LettersToComplete == LettersToComplete.Consonants || LettersToComplete == LettersToComplete.All)
        {
            if (Word.Contains("ca") || Word.Contains("co") || Word.Contains("cu"))
                letters += "kq";
            if (Word.Contains("ce") || Word.Contains("ci"))
                letters += "s";
            if (Word.Contains("f"))
                letters += "v";
            if (Word.Contains("k"))
                letters += "cq";
            if (Word.Contains("l"))
                letters += "u";
            if (Word.Contains("q"))
                letters += "ck";
            if (Word.Contains("s"))
                letters += "cçz";
            if (Word.Contains("w"))
                letters += "uv";
            if (Word.Contains("x"))
                letters += "ch";
            if (Word.Contains("y"))
                letters += "i";
            if (Word.Contains("z"))
                letters += "s";
            if (Word.Contains("ç"))
                letters += "cs";
            if (Word.Contains("m"))
                letters += "n";
        }
        else if (LettersToComplete == LettersToComplete.Encounters)
        {
            if (Word.Contains("ch"))
                letters += "x";
            if (Word.Contains("ss"))
                letters += "ç";
            if (Word.Contains("sc"))
                letters += "ç";
            if (Word.Contains("m"))
                letters += "n";
        }
        if (/*LettersToComplete == LettersToComplete.Encounters || */LettersToComplete == LettersToComplete.All)
        {
            if (Word.Contains("a"))
                letters += "áã";
            if (Word.Contains("á"))
                letters += "aã";
            if (Word.Contains("ã"))
                letters += "aá";
            if (Word.Contains("e"))
                letters += "é";
            if (Word.Contains("é"))
                letters += "e";
            if (Word.Contains("i"))
                letters += "í";
            if (Word.Contains("í"))
                letters += "i";
            if (Word.Contains("o"))
                letters += "óõ";
            if (Word.Contains("ó"))
                letters += "oõ";
            if (Word.Contains("õ"))
                letters += "oó";
            if (Word.Contains("u"))
                letters += "ú";
            if (Word.Contains("ú"))
                letters += "u";
        }

        string let = string.Empty;
        foreach (var c in letters)
        {
            if (let.IndexOf(c) == -1)
                let += c;
        }
        letters = let;

        for (int i = 0; i < LetterHolders.Count; i++)
        {
            if (!string.IsNullOrEmpty(LetterHolders[i].Value))
                l.Replace(LetterHolders[i].Value.ToLower(), "");
        }

        string missingLetters = Boat.LettersMissing;
        string ml = string.Empty;
        foreach (var c in missingLetters)
        {
            if (ml.IndexOf(c) == -1)
                ml += c;
        }
        missingLetters = ml;

        return letters;
    }

    private string GetAnyLetter(string letters = "abcdefghijklmnopqrstuvwxyzçáãéíóõú")
    {
        string l = GetSimilarLetters(letters);

        string lettersMissing = Boat.LettersMissing;

        bool anyLetterExists = false;
        List<SeaLetter> holders = new List<SeaLetter>(LetterHolders);
        for (int i = 0; i < holders.Count; i++)
        {
            if (string.IsNullOrEmpty(holders[i].Value.ToLower()))
                continue;

            if (lettersMissing.Contains(holders[i].Value.ToLower()))
            {
                //lettersMissing = lettersMissing.Replace(LetterHolders[i].Value.ToLower(), "");                
                lettersMissing = lettersMissing.Remove(lettersMissing.LastIndexOf(holders[i].Value.ToLower()), 1);

                anyLetterExists = true;
            }
            
            l = l.Replace(holders[i].Value.ToLower(), "");
        }

        if (l.Length <= 0)
        {
            for (int i = lettersMissing.Length; i < 5; i++)
            {
                int letterIndex = Random.Range(0, letters.Length);
                l += letters[letterIndex];
                letters.Replace(letters[letterIndex].ToString(), string.Empty);
            }
        }

        int probWordLetter = Random.Range(0, 100);
        if ((anyLetterExists == false || probWordLetter <= ChanceOfWordLetter) && !string.IsNullOrEmpty(lettersMissing))
            return lettersMissing[Random.Range(0, lettersMissing.Length)].ToString();
        else
            return l[Random.Range(0, l.Length)].ToString();
    }

    public void LetterReleased(SeaLetter letter)
    {
        if(letter.Letter.CollidingSlot != null)
        {
            string color = string.Empty;
            string l = letter.Value;
            LetterSlot slot = letter.Letter.CollidingSlot;
            if (letter.Letter.CollidingSlot.Value.ToLower() == letter.Value.ToLower())
            {
                letter.CorrectSlot(() => CheckWordCompletion());
                //CheckWordCompletion();
                color = RightColor;
                _rightAnswers++;
                _wordRightAnswers++;
            }
            else
            {
                letter.ReturnToPosition();
                color = WrongColor;
                _wrongAnswers++;
                _wordWrongAnswers++;
            }
            for (int i = 0; i < Boat.LetterSlots.Count; i++)
            {
                if (Boat.LetterSlots[i] == slot)
                {
                    LogPlay("<color=#" + color + ">" + l.ToUpper() + "</color>");                
                }
                else if (Boat.LetterSlots[i].IsFilled)
                    LogPlay(Boat.LetterSlots[i].Value.ToUpper());
                else
                    LogPlay("_");
            }
            LogPlay("\n");
            print(_report);
            //CheckWordCompletion();
        }
        else
        {
            letter.ReturnToPosition();
        }
    }

    private void EndGame()
    {
        //BackToMainMenu();
        PotaTween.Get(Boat.gameObject, 1).Reverse(() => { Boat.gameObject.SetActive(false); });
        if (Holders.gameObject.activeSelf)
            Holders.gameObject.SetActive(false);
        LetterSpawner.gameObject.SetActive(false);
        Hint.gameObject.SetActive(false);

        _gameEndTime = System.DateTime.Now;
        _gameTotalTime = _gameEndTime.Subtract(_gameStartTime);

        string t = string.Format("{0:D2}m {1:D2}s",
        (int)_gameTotalTime.TotalMinutes,
        _gameTotalTime.Seconds);

        LogPlay("\n\nTempo total da atividade: " + t + "\n");
        LogPlay("Total de tentativas corretas: " + _rightAnswers + "\nTotal de tentativas erradas: " + _wrongAnswers);

        ReportScroll.ContentText.text = _report;
        ReportScroll.SetActive(true);
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void LogPlay(string log)
    {
        _report += log;
    }
}
