﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReportScroll : MonoBehaviour {

    public Text ContentText;
    public Button SendMailButton;

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = PotaTween.Get(gameObject);
            return _tween;
        }
    }

    void Awake()
    {
        SendMailButton.onClick.AddListener(() => 
        {
            SetActive(false);
            SeaGameController.Instance.SendMailPopup.SetActive(true);
        });
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Stop();
            Tween.Play();
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);
            });
        }
    }
}
