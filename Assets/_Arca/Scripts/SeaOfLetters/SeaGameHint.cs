﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeaGameHint : MonoBehaviour {
    
    public Image HintImage;

    public void SetImage(Sprite sprite)
    {
        HintImage.enabled = sprite != null;
        HintImage.sprite = sprite;
        HintImage.preserveAspect = true;
    }
}
