﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterSlot : MonoBehaviour {
    
    public string Value;

    public bool IsFilled { get; set; }

    private Collider2D _collider;
    public Collider2D Collider
    {
        get
        {
            if (_collider == null)
                _collider = GetComponent<Collider2D>();

            return _collider;
        }
    }

    private PotaTween _letterPlacedTween;
    public PotaTween LetterPlacedTween
    {
        get
        {
            if (_letterPlacedTween == null)
            {
                _letterPlacedTween = PotaTween.Create(gameObject, 1).
                    SetScale(Vector3.one * 0.75f, Vector3.one).
                    SetEaseEquation(Ease.Equation.OutElastic);
            }
            return _letterPlacedTween;
        }
    }

    void Start()
    {
        Invoke("SetColliderScale", 0.1f);
    }

    void SetColliderScale()
    {
        float scale = GetComponent<RectTransform>().rect.width / (GetComponent<Image>().sprite.bounds.size.x * 100);
        scale = Mathf.Clamp01(scale);

        ((CircleCollider2D)Collider).radius *= scale;
    }

    public void DestroySelf(float delay = 0)
    {
        PotaTween.Create(gameObject, "Destroy").
            SetScale(Vector3.one, Vector3.zero).
            SetAlpha(1f, 0f).
            SetEaseEquation(Ease.Equation.InBack).
            SetDuration(0.5f).
            SetDelay(delay).
            Play(() => 
            {
                Destroy(gameObject);
            });
    }
}
