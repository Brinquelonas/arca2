﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TSVReader : MonoBehaviour {

    public TextAsset TSV;

    private List<List<string>> _words = new List<List<string>>();

    public List<string> Words
    {
        get
        {
            ReadTSV();

            switch (SeaGameController.Instance.LettersToComplete)
            {
                default:
                case LettersToComplete.None:
                    return null;
                case LettersToComplete.Vowels:
                    return _words[0];
                case LettersToComplete.Consonants:
                    return _words[1];
                case LettersToComplete.Encounters:
                    return _words[2];
                case LettersToComplete.All:
                    return _words[3];
            }
        }
    }

    private void ReadTSV()
    {
        _words = new List<List<string>>() { new List<string>(), new List<string>(), new List<string>(), new List<string>()};

        string tsv = TSV.text;

        string[] lines = tsv.Split('\n');

        for (int i = 1; i < lines.Length; i++)
        {
            string[] columns = lines[i].Split('\t');

            for (int j = 0; j < columns.Length; j++)
            {
                if (string.IsNullOrEmpty(columns[j].Trim()) || columns[j].Length <= 1)
                    continue;

                _words[j].Add(columns[j].Trim());

                if (!_words[3].Contains(columns[j]))
                    _words[3].Add(columns[j].Trim());
            }
        }        
    }
}
