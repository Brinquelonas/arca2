﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeaLetter : MonoBehaviour {

    [SerializeField]
    private string _value;
    public string Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value.ToUpper();
            if (Letter != null)
                Letter.Value = _value;
        }
    }

    public DraggableLetter Letter;
    public RectTransform LetterPlaceHolder;

    private RectTransform _rectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();
            return _rectTransform;
        }
    }

    private PotaTween _letterReturnedAnimation;
    public PotaTween LetterReturnedAnimation
    {
        get
        {
            if (_letterReturnedAnimation == null)
            {
                _letterReturnedAnimation = PotaTween.Create(gameObject, 1).
                SetPosition(TweenAxis.Y, RectTransform.anchoredPosition.y - 50, RectTransform.anchoredPosition.y).
                SetEaseEquation(Ease.Equation.OutElastic);
            }
            return _letterReturnedAnimation;
        }
    }

    void Awake()
    {
//TEST-------------------------------------------
        Value = _value;

        if (Letter != null)
        {
            Letter.OnBegin.AddListener((letter) => 
            {
                Letter.transform.SetParent(SeaGameController.Instance.DraggingLettersHolder);
            });

            Letter.OnEnd.AddListener((letter) => 
            {
                Letter.transform.SetParent(LetterPlaceHolder);
                SeaGameController.Instance.LetterReleased(this);
            });
        }
//TEST-------------------------------------------
    }

    public void CreateNewLetter(string letter)
    {
        if (Letter == null)
        {
            Letter = Instantiate(SeaGameController.Instance.LetterPrefab, LetterPlaceHolder);
            Letter.RectTransform.anchoredPosition = Vector2.zero;

            Letter.OnBegin.AddListener((l) =>
            {
                Letter.transform.SetParent(SeaGameController.Instance.DraggingLettersHolder);
            });

            Letter.OnEnd.AddListener((l) =>
            {
                Letter.transform.SetParent(LetterPlaceHolder);
                SeaGameController.Instance.LetterReleased(this);
            });
        }       

        Value = letter;

        PotaTween.Create(Letter.gameObject, "Appear").
            SetScale(Vector3.zero, Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play();
    }

    public void CreateNewLetter(bool sameLetter = true)
    {
        if (!sameLetter)
        {
            CreateNewLetter(SeaGameController.Instance.GetNewLetter());
        }
        else
        {
            CreateNewLetter(Value);
        }
    }

    public void CorrectSlot(System.Action callback = null)
    {
        Letter.Interactable = false;
        Letter.CollidingSlot.Collider.enabled = false;
        Letter.CollidingSlot.IsFilled = true;
        Letter.transform.SetParent(Letter.CollidingSlot.transform);

        float scale = Letter.CollidingSlot.GetComponent<RectTransform>().sizeDelta.x / (Letter.CollidingSlot.GetComponent<Image>().sprite.bounds.size.x * 100);
        scale = Mathf.Clamp01(scale);

        LetterSlot slot = Letter.CollidingSlot;

        PotaTween.Create(Letter.gameObject, "Fit").
            SetPosition(Letter.GetComponent<RectTransform>().anchoredPosition, Vector3.zero).//LetterPlaceHolder.anchoredPosition).
            SetScale(Vector3.one, new Vector3(scale, scale, 1)).
            SetSpeed(2000f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() => slot.LetterPlacedTween.Play(callback));

        Letter = null;
        if (SeaGameController.Instance.LettersToComplete != LettersToComplete.Vowels)
            Value = "";

        CreateNewLetter(SeaGameController.Instance.LettersToComplete == LettersToComplete.Vowels);
    }

    public void ReturnToPosition(System.Action callback = null)
    {
        PotaTween.Create(Letter.gameObject, "Return").
            SetPosition(Letter.GetComponent<RectTransform>().anchoredPosition, Vector3.zero).//LetterPlaceHolder.anchoredPosition).
            SetSpeed(2000f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() => 
            {
                if (callback != null)
                    callback();

                PlayLetterReturnedAnimation();
            });
    }

    public void PlayLetterReturnedAnimation()
    {
        LetterReturnedAnimation.Stop();
        LetterReturnedAnimation.Play();
    }
}
