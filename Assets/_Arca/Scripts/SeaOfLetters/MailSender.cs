﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using UnityEngine;
using UnityEngine.Events;

public class MailSender {

    [System.Serializable]
    public class MailEvent : UnityEvent { }

    public MailEvent OnSuccess = new MailEvent();
    public MailEvent OnFail = new MailEvent();

    private System.Action _callback;

    public IEnumerator SendMailCoroutine(string toMail, string body)
    {
        yield return null;

        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("mardeletras@brinquelonas.com.br");
        mail.To.Add(toMail);
        mail.Subject = Application.productName + " " + System.DateTime.Now;
        mail.IsBodyHtml = true;

        //mail.Body = "Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body;
        body = "Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body;
        mail.Body = body.Replace("\n", "<br/>");

        SmtpClient smtpServer = new SmtpClient("smtp.brinquelonas.com.br");
        smtpServer.Port = 587;
        smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("mardeletras@brinquelonas.com.br", "RealidadeAumentada2");
        smtpServer.EnableSsl = true;
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chai, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

        bool success = true;
        try
        {
            smtpServer.Send(mail);
        }
        catch (System.Exception e)
        {
            success = false;
            Debug.Log(e.GetBaseException());
            //return false;
            OnFail.Invoke();
        }

        if (success)
        {
            OnSuccess.Invoke();
            yield break;
        }

        //return true;
        yield return null;
    }

    private void SendComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        string token = (string)e.UserState;
        
        if (e.Cancelled)
            OnFail.Invoke();
        if (e.Error != null)
            OnFail.Invoke();
        else
            OnSuccess.Invoke();
    }

}
