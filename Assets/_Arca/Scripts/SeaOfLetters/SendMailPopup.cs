﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SendMailPopup : MonoBehaviour
{
    public InputField EmailInputField;
    public GameObject Feedback;
    public Button Button;

    private MailSender MailSender = new MailSender();

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            return _tween;
        }
    }

    private void SendMailButtonClicked(string body, System.Action successCallback, System.Action failCallback)
    {
        if (string.IsNullOrEmpty(EmailInputField.text))
            return;

        Feedback.SetActive(true);

        MailSender.OnSuccess.RemoveAllListeners();
        MailSender.OnSuccess.AddListener(() =>
        {
            successCallback();
        });

        MailSender.OnFail.RemoveAllListeners();
        MailSender.OnFail.AddListener(() =>
        {
            failCallback();
        });

        //MailSender.SendMail(EmailInputField.text, body);
        StartCoroutine(MailSender.SendMailCoroutine(EmailInputField.text, body));
    }

    public void SetActive(bool active, System.Action callback = null)
    {
        if (active)
        {
            Feedback.SetActive(false);
            gameObject.SetActive(active);
            Tween.Stop();
            Tween.Play(callback);
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);

                if (callback != null)
                    callback();
            });
        }
    }

    public void SendMail()
    {
        SendMailButtonClicked(SeaGameController.Instance.Log, () =>
        {
            SetActive(false);
            SeaGameController.Instance.AlertPopup.Text.text = "E-mail enviado com sucesso!\nEnviar o relatório para outro e-mail?".ToUpper();
            SeaGameController.Instance.AlertPopup.SetActive(true, 1);
            SeaGameController.Instance.AlertPopup.Buttons[0].onClick.AddListener(SeaGameController.Instance.BackToMainMenu);
            SeaGameController.Instance.AlertPopup.Buttons[1].onClick.AddListener(() =>
            {
                SeaGameController.Instance.AlertPopup.SetActive(false);
                SetActive(true);
            });
        }, () =>
        {
            SetActive(false);
            SeaGameController.Instance.AlertPopup.Text.text = "Falha ao enviar e-mail! Verifique sua conexão com a internet e se o e-mail foi digitado corretamente!".ToUpper();
            SeaGameController.Instance.AlertPopup.SetActive(true);
            SeaGameController.Instance.AlertPopup.Buttons[0].onClick.AddListener(() =>
            {
                SetActive(true);
                Button.gameObject.SetActive(true);
                SeaGameController.Instance.AlertPopup.SetActive(false);
            });
        });
    }
}
