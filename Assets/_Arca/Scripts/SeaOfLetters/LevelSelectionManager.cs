﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameConfigs
{
    public static string PlayerName = string.Empty;
    public static int GameLevel = 0;
    public static LettersToComplete LettersToComplete = LettersToComplete.None;
}

public class LevelSelectionManager : MonoBehaviour {

    public Button ButtonLevel1;
    public Button ButtonLevel2;
    public Button ButtonLevel3;
    public Button ButtonLevel4;

    void Start()
    {
        GameConfigs.PlayerName = string.Empty;

        ButtonLevel1.onClick.AddListener(() => 
        {
            GameConfigs.LettersToComplete = LettersToComplete.Vowels;
            GameConfigs.GameLevel = 1;
            LoadScene("SeaGame");
        });
        ButtonLevel2.onClick.AddListener(() =>
        {
            GameConfigs.LettersToComplete = LettersToComplete.Consonants;
            GameConfigs.GameLevel = 2;
            LoadScene("SeaGame");
        });
        ButtonLevel3.onClick.AddListener(() =>
        {
            GameConfigs.LettersToComplete = LettersToComplete.Encounters;
            GameConfigs.GameLevel = 3;
            LoadScene("SeaGame");
        });
        ButtonLevel4.onClick.AddListener(() =>
        {
            GameConfigs.LettersToComplete = LettersToComplete.All;
            GameConfigs.GameLevel = 4;
            LoadScene("SeaGame");
        });
    }

	public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}
