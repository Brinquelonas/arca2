﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaLetterSpawner : MonoBehaviour {

    private RectTransform _rectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (_rectTransform == null)
                _rectTransform = GetComponent<RectTransform>();
            return _rectTransform;
        }
    }

    public void StartSpawning()
    {
        StartCoroutine(SpawnLoop());
    }

    public void StopSpawning()
    {
        StopCoroutine(SpawnLoop());
    }

    IEnumerator SpawnLoop()
    {
        while (true)
        {
            SpawnLetter(SeaGameController.Instance.GetNewLetter());
            yield return new WaitForSeconds(2f);
        }
    }

	public void SpawnLetter(string letter)
    {
        Vector2 position = new Vector2();
        position.x = Random.Range(RectTransform.rect.xMin, RectTransform.rect.xMax);
        position.y = Random.Range(RectTransform.rect.yMin, RectTransform.rect.yMax);

        SeaLetter letterHolder = Instantiate(SeaGameController.Instance.LetterHolderPrefab, RectTransform);
        letterHolder.GetComponent<RectTransform>().anchoredPosition = position;
        letterHolder.Value = letter;

        letterHolder.CreateNewLetter(letter);

        InitializeLetterHolder(letterHolder);
    }

    private void InitializeLetterHolder(SeaLetter letterHolder)
    {
        SeaGameController.Instance.Holders.List.Add(letterHolder);

        PotaTween.Create(letterHolder.gameObject, 8).
            SetPosition(letterHolder.GetComponent<RectTransform>().anchoredPosition, letterHolder.GetComponent<RectTransform>().anchoredPosition + new Vector2(3000, 0)).
            SetSpeed(200).
            Play(() =>
            {
                Destroy(letterHolder.gameObject);
                SeaGameController.Instance.Holders.List.Remove(letterHolder);
            });
    }
}
