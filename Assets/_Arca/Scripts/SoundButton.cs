﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour {

    private Button _button;
    private AudioSource _source;

    void Start ()
    {
        _button = GetComponent<Button>();
        _source = GetComponent<AudioSource>();

        if(_source.clip != null)
        {
            _button.onClick.AddListener(() =>
            {
                _button.interactable = false;
                _source.Play();

                Invoke("EnableButton", _source.clip.length);
            });
        }
    }
	
	void Update ()
    {
		
	}

    void EnableButton()
    {
        _button.interactable = true;
    }
}
