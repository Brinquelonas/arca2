﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AnimalInfo
{
    public string Name;
    public List<string> Texts;

    public AnimalInfo(string name, List<string> texts)
    {
        Name = name;
        Texts = texts;
    }
}

public class QuestionsTSVReader : MonoBehaviour {

    public TextAsset TSV;
    public List<AnimalInfo> Animals = new List<AnimalInfo>();

    private static QuestionsTSVReader _instance;
    public static QuestionsTSVReader Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<QuestionsTSVReader>();

            return _instance;
        }
    }

    private void Awake()
    {
        ReadTSV();
    }

    public void ReadTSV()
    {
        string[] lines = TSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);

            string name = contents[0].Trim();
            
            List<string> texts = new List<string>();
            for (int j = 0; j < 5; j++)
            {
                int index = 1 + j;
                if (!string.IsNullOrEmpty(contents[index].Trim()))
                    texts.Add(contents[index].Trim());
            }

            Animals.Add(new AnimalInfo(name, texts));
        }
    }
    
    public AnimalInfo GetAnimalInfo(string name)
    {
        return Animals.Find((a) => a.Name == name);
    }
    
}
