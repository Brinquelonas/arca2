﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterController2D : MonoBehaviour {

    public Text LetterText;
    public Button CloseButton;

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = LetterText.GetComponent<Button>();
            return _button;
        }
    }

    private AudioSource _source;
    public AudioSource AudioSource
    {
        get
        {
            if (_source == null)
                _source = GetComponent<AudioSource>();
            if (_source == null)
            {
                _source = gameObject.AddComponent<AudioSource>();
                _source.spatialBlend = 0;
                _source.pitch = 1.05f;
            }
            return _source;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = PotaTween.Create(gameObject).
                    SetScale(Vector3.one * 0.9f, Vector3.one).
                    SetAlpha(0f, 1f).
                    SetDuration(0.3f);
            return _tween;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            if (AudioSource.isPlaying)
                AudioSource.Stop();
            AudioSource.Play();
        });

        CloseButton.onClick.AddListener(() =>
        {
            SetActive(false);
        });
    }

    public void SetLetter(string letter)
    {
        LetterText.text = letter;
        AudioSource.clip = Resources.Load<AudioClip>("Audio/" + letter.ToLower());
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Stop();
            Tween.Play();
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                gameObject.SetActive(false);
            });
        }
    }
}
