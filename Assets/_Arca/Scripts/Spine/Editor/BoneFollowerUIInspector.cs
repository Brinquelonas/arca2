﻿

using UnityEditor;
using UnityEngine;

namespace Spine.Unity.Editor
{
    [CustomEditor(typeof(SpineUIBoneFollower)), CanEditMultipleObjects]
    public class BoneFollowerUIInspector : UnityEditor.Editor
    {
        SerializedProperty boneName, skeletonRenderer, followZPosition, followBoneRotation, rotationTolerance, followLocalScale, followSkeletonFlip;
        SpineUIBoneFollower targetBoneFollower;
        bool needsReset;

        #region Context Menu Item
        [MenuItem("CONTEXT/SkeletonRenderer/Add UIBoneFollower GameObject")]
        static void AddBoneFollowerGameObject(MenuCommand cmd)
        {
            var skeletonRenderer = cmd.context as SkeletonGraphic;
            var go = new GameObject("BoneFollower");
            var t = go.transform;
            t.SetParent(skeletonRenderer.transform);
            t.localPosition = Vector3.zero;

            var f = go.AddComponent<SpineUIBoneFollower>();
            f.skeletonRenderer = skeletonRenderer;

            EditorGUIUtility.PingObject(t);

            Undo.RegisterCreatedObjectUndo(go, "Add BoneFollower");
        }

        // Validate
        [MenuItem("CONTEXT/SkeletonRenderer/Add UIBoneFollower GameObject", true)]
        static bool ValidateAddBoneFollowerGameObject(MenuCommand cmd)
        {
            var skeletonRenderer = cmd.context as SkeletonGraphic;
            return skeletonRenderer.IsValid;
        }
        #endregion

        void OnEnable()
        {
            skeletonRenderer = serializedObject.FindProperty("skeletonRenderer");
            boneName = serializedObject.FindProperty("boneName");
            followBoneRotation = serializedObject.FindProperty("followBoneRotation");
            rotationTolerance = serializedObject.FindProperty("rotationTolerance");
            followZPosition = serializedObject.FindProperty("followZPosition");
            followLocalScale = serializedObject.FindProperty("followLocalScale");
            followSkeletonFlip = serializedObject.FindProperty("followSkeletonFlip");

            targetBoneFollower = (SpineUIBoneFollower)target;
            if (targetBoneFollower.SkeletonRenderer != null)
                targetBoneFollower.SkeletonRenderer.Initialize(false);

            if (!targetBoneFollower.valid || needsReset)
            {
                targetBoneFollower.Initialize();
                targetBoneFollower.LateUpdate();
                needsReset = false;
                SceneView.RepaintAll();
            }
        }

        public void OnSceneGUI()
        {
            var tbf = target as SpineUIBoneFollower;
            var skeletonRendererComponent = tbf.skeletonRenderer;
            if (skeletonRendererComponent == null) return;

            var transform = skeletonRendererComponent.transform;
            var skeleton = skeletonRendererComponent.Skeleton;

            if (string.IsNullOrEmpty(tbf.boneName))
            {
                SpineHandles.DrawBones(transform, skeleton);
                SpineHandles.DrawBoneNames(transform, skeleton);
                Handles.Label(tbf.transform.position, "No bone selected", EditorStyles.helpBox);
            }
            else
            {
                var targetBone = tbf.bone;
                if (targetBone == null) return;
                SpineHandles.DrawBoneWireframe(transform, targetBone, SpineHandles.TransformContraintColor);
                Handles.Label(targetBone.GetWorldPosition(transform), targetBone.Data.Name, SpineHandles.BoneNameStyle);
            }
        }

        override public void OnInspectorGUI()
        {
            if (serializedObject.isEditingMultipleObjects)
            {
                if (needsReset)
                {
                    needsReset = false;
                    foreach (var o in targets)
                    {
                        var bf = (SpineUIBoneFollower)o;
                        bf.Initialize();
                        bf.LateUpdate();
                    }
                    SceneView.RepaintAll();
                }

                EditorGUI.BeginChangeCheck();
                DrawDefaultInspector();
                needsReset |= EditorGUI.EndChangeCheck();
                return;
            }

            if (needsReset && UnityEngine.Event.current.type == EventType.Layout)
            {
                targetBoneFollower.Initialize();
                targetBoneFollower.LateUpdate();
                needsReset = false;
                SceneView.RepaintAll();
            }
            serializedObject.Update();

            // Find Renderer
            if (skeletonRenderer.objectReferenceValue == null)
            {
                SkeletonGraphic parentRenderer = targetBoneFollower.GetComponentInParent<SkeletonGraphic>();
                if (parentRenderer != null && parentRenderer.gameObject != targetBoneFollower.gameObject)
                {
                    skeletonRenderer.objectReferenceValue = parentRenderer;
                    Debug.Log("Inspector automatically assigned BoneFollower.SkeletonRenderer");
                }
            }

            EditorGUILayout.PropertyField(skeletonRenderer);
            var skeletonRendererReference = skeletonRenderer.objectReferenceValue as SkeletonGraphic;
            if (skeletonRendererReference != null)
            {
                if (skeletonRendererReference.gameObject == targetBoneFollower.gameObject)
                {
                    skeletonRenderer.objectReferenceValue = null;
                    EditorUtility.DisplayDialog("Invalid assignment.", "BoneFollower can only follow a skeleton on a separate GameObject.\n\nCreate a new GameObject for your BoneFollower, or choose a SkeletonRenderer from a different GameObject.", "Ok");
                }
            }

            if (targetBoneFollower.valid)
            {
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(boneName);
                needsReset |= EditorGUI.EndChangeCheck();

                EditorGUILayout.PropertyField(followBoneRotation);
                EditorGUILayout.PropertyField(rotationTolerance);
                EditorGUILayout.PropertyField(followZPosition);
                EditorGUILayout.PropertyField(followLocalScale);
                EditorGUILayout.PropertyField(followSkeletonFlip);
            }
            else
            {
                var boneFollowerSkeletonRenderer = targetBoneFollower.skeletonRenderer;
                if (boneFollowerSkeletonRenderer == null)
                {
                    EditorGUILayout.HelpBox("SkeletonRenderer is unassigned. Please assign a SkeletonRenderer (SkeletonAnimation or SkeletonAnimator).", MessageType.Warning);
                }
                else
                {
                    boneFollowerSkeletonRenderer.Initialize(false);

                    if (boneFollowerSkeletonRenderer.skeletonDataAsset == null)
                        EditorGUILayout.HelpBox("Assigned SkeletonRenderer does not have SkeletonData assigned to it.", MessageType.Warning);

                    if (!boneFollowerSkeletonRenderer.IsValid)
                        EditorGUILayout.HelpBox("Assigned SkeletonRenderer is invalid. Check target SkeletonRenderer, its SkeletonDataAsset or the console for other errors.", MessageType.Warning);
                }
            }

            var current = UnityEngine.Event.current;
            bool wasUndo = (current.type == EventType.ValidateCommand && current.commandName == "UndoRedoPerformed");
            if (wasUndo)
                targetBoneFollower.Initialize();

            serializedObject.ApplyModifiedProperties();
        }
    }

}
