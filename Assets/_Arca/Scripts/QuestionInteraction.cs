﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using TMPro;

public class QuestionInteraction : MonoBehaviour {

    private AudioSource _audioSource;
    public AudioSource AudioSource
    {
        get
        {
            if (_audioSource == null)
                _audioSource = GetComponent<AudioSource>();
            if (_audioSource == null)
                _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.spatialBlend = 0;
            _audioSource.playOnAwake = false;

            return _audioSource;
        }
    }

    public Image Balloon;
    public Button SoundButton;
	public TextMeshProUGUI Name;
	public TextMeshProUGUI Text;
    public AnimalAnimations Animals;
    public string ResourcesPath = "";
    
    private AnimalInfo _currentAnimal;
    private int _textIndex;

    public int Space { get; set; }
    public int CorrectAnswer { get; set; }
    

	protected void Awake()
	{
		
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            OnHandlerFound();
        }
    }

    public void OnHandlerFound()
	{
        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

        //Question.text = _currentQuestion.Text;
        Balloon.gameObject.SetActive(false);
	}

    public void OnAnimalFound(string animal)
    {
        if (gameObject.activeSelf)
            return;

        gameObject.SetActive(true);

        _currentAnimal = QuestionsTSVReader.Instance.GetAnimalInfo(animal);

        Name.text = _currentAnimal.Name;
        Animals.EnableAnimation(animal);

        SoundButton.interactable = true;
        SoundButton.gameObject.SetActive(Animals.GetAudioClip(_currentAnimal.Name) != null);

        _textIndex = 0;
        Text.text = _currentAnimal.Texts[_textIndex];
        PlayAnimalText();

        Balloon.gameObject.SetActive(false);
        ShowBalloon();
    }

    public void NextText()
    {
        _textIndex++;
        _textIndex %= _currentAnimal.Texts.Count;
        Text.text = _currentAnimal.Texts[_textIndex];
        PlayAnimalText();
    }

    public void PlayAnimalText()
    {
        AudioClip clip = Animals.GetTextsAudioClip(_currentAnimal.Name, _textIndex);
        PlayAudio(clip);
    }

    public void PlayAnimalAudio()
    {
        AudioClip clip = Animals.GetAudioClip(_currentAnimal.Name);
        PlayAudio(clip);

        DisableAudioButton();
        Invoke("EnableAudioButton", clip.length);
    }

    private void DisableAudioButton()
    {
        SoundButton.interactable = false;
    }

    private void EnableAudioButton()
    {
        SoundButton.interactable = true;
    }

    private void PlayAudio(AudioClip clip, System.Action callback = null)
    {
        if (clip == null)
        {
            if (callback != null) callback();
            return;
        }

        if (AudioSource.isPlaying)
            AudioSource.Stop();

        AudioSource.clip = clip;
        AudioSource.Play();

        if (callback != null)
            StartCoroutine(AudioCallback(clip, callback));
    }

    private void ShowBalloon(System.Action callback = null)
    {
        Balloon.gameObject.SetActive(true);

        PotaTween.Create(Balloon.gameObject).Stop();
        PotaTween.Create(Balloon.gameObject).
            SetAlpha(0f, 1f).
            SetScale( new Vector3(0.8f, 0.8f, 1), Vector3.one).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutBack).
            Play(callback);
    }

    public void HideAll()
    {
        //HideElement(Balloon.gameObject, () => gameObject.SetActive(false));

        gameObject.SetActive(false);
    }

    private void HideElement(GameObject element, System.Action callback = null)
    {
        PotaTween.Create(element, 1).Stop();
        PotaTween.Create(element, 1).
            SetAlpha(1f, 0f).
            SetDuration(0.5f).
            SetEaseEquation(Ease.Equation.OutSine).
            Play(() =>
            {
                if (callback != null)
                    callback();

                element.SetActive(false);
            });
    }

    private IEnumerator AudioCallback(AudioClip clip, System.Action callback)
    {
        float elapsedTime = 0;

        while(elapsedTime < clip.length)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        callback();
    }
}
