﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class MainMenuCanvas : MonoBehaviour {
    
    public Button PlayGame;
    public Button ARGame;
    public Button SoundGame;
    public Button QuitButton;

    public PotaTween PlayGameTween;
    public PotaTween ArGameTween;
    public PotaTween SoundGameTween;
    public PotaTween Fader;

    public GameObject LoadingText;

    void OnEnable()
    {
        Invoke("Open", 0.01f);
        //Open();
    }

    void Start ()
    {
        PlayGame.onClick.AddListener(() =>
        {
            PlayGameTween.Reverse();
            SoundGameTween.Reverse();
            ArGameTween.Reverse();
            Fader.Reverse(() => SceneManager.LoadScene("LevelSelection"));
        });

        ARGame.onClick.AddListener(() =>
        {
            PlayGameTween.Reverse();
            ArGameTween.Reverse();
            SoundGameTween.Reverse();
            //Fader.Reverse(() => SceneManager.LoadScene("ArGame"));
            Fader.Reverse(() => StartCoroutine(LoadSceneAsync("ArGame")));
        });

        SoundGame.onClick.AddListener(() =>
        {
            PlayGameTween.Reverse();
            ArGameTween.Reverse();
            SoundGameTween.Reverse();
            Fader.Reverse(() => SceneManager.LoadScene("SoundGame"));
        });

        QuitButton.onClick.AddListener(() => 
        {
            PlayGameTween.Reverse();
            ArGameTween.Reverse();
            SoundGameTween.Reverse();
            Fader.Reverse(() => Application.Quit());
        });
    }
	
	void Update () {
		
	}

    void Open()
    {
        PlayGameTween.Play();
        ArGameTween.Play();
        SoundGameTween.Play();
        Fader.Play();
    }

    IEnumerator LoadSceneAsync(string sceneName)
    {
        var async = SceneManager.LoadSceneAsync(sceneName);

        LoadingText.SetActive(true);

        while (!async.isDone)
            yield return null;
    }
}
