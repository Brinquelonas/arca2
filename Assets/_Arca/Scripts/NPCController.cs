﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class NPCController : MonoBehaviour {

    public SkeletonGraphic NPCGraphic;
    public SkeletonGraphic MouthGraphic;

    public string CurrentAnimationState { get; set; }

    public void PlayAnimation(string animationName)
    {
        PlayAnimation(animationName, false, null);
    }

    public void PlayAnimationLoop(string animationName)
    {
        PlayAnimation(animationName, true, null);
    }

    public void PlayAnimation(string animationName, bool loop, System.Action callback = null)
    {
        CurrentAnimationState = animationName;
        Spine.TrackEntry track = NPCGraphic.AnimationState.SetAnimation(0, animationName, loop);
        track.Complete += (t) =>
        {
            if (callback != null)
                callback();
        };
    }

    public void PlayMouthAnimation(float audioLength, System.Action callback = null)
    {
        StartCoroutine(MouthAnimation(audioLength, callback));
    }

    public void PlayMouthAnimation(string animationName, System.Action callback = null)
    {
        Spine.TrackEntry track = MouthGraphic.AnimationState.SetAnimation(1, animationName, true);
        track.Complete += (t) =>
        {
            if (callback != null)
                callback();
        };
    }

    private IEnumerator MouthAnimation(float audioLength, System.Action callback)
    {
        PlayMouthAnimation(CurrentAnimationState);
        MouthGraphic.AnimationState.TimeScale = 1;

        float elapsedTime = 0;
        while(elapsedTime < audioLength)
        {
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        if (callback != null)
            callback();
    }
}
