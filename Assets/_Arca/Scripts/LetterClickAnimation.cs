﻿using UnityEngine;

public class LetterClickAnimation : MonoBehaviour
{
    public bool UseScale = true;
    public bool UseColor = true;

    [Header("Normal")]
    public Color NormalColor = Color.white;
    public Vector3 NormalScale = new Vector3(1, 1, 1);

    [Header("Clicked")]
    public Color ClickedColor = Color.gray;
    public Vector3 ClickedScale = new Vector3(0.9f, 0.9f, 0.9f);

    private PotaTween _clickAnimation;

    void Start ()
    {
		_clickAnimation = PotaTween.Create(gameObject, 10);

        if(UseScale)
            _clickAnimation.SetScale(NormalScale, ClickedScale);
        if(UseColor)
            _clickAnimation.SetColor(NormalColor, ClickedColor);

        _clickAnimation.SetDuration(0.1f);
    }

    void Update ()
    {
		
	}

    public void Down()
    {
        _clickAnimation.Stop();
        _clickAnimation.Play();
    }

    public void Up()
    {
        _clickAnimation.Stop();
        _clickAnimation.Reverse();
    }
}
